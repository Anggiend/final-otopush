package com.automation.PushNotif;

import java.io.FileInputStream;
import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@SpringBootApplication
public class PushNotifApplication {
	
	public static void main(String[] args) {
		// Initialize Firebase
        try {
            // [START initialize]
            FileInputStream serviceAccount = new FileInputStream("src/main/resources/static/service-account.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://mynotify-f13cb.firebaseio.com")
                    .build();
            FirebaseApp.initializeApp(options);
            // [END initialize]
        } catch (IOException e) {
            System.out.println("ERROR: invalid service account credentials. See README.");
            System.out.println(e.getMessage());

            System.exit(1);
        }
        
		SpringApplication.run(PushNotifApplication.class, args);
	}
}
