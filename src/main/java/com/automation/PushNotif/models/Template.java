package com.automation.PushNotif.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "template")
public class Template {
	@Id
	@Column(name = "id_template")
	private Integer id_template;

	@Autowired
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sender")
	public Device sender;
	
	@Autowired
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="receiver")
	public Device receiver;

	@Column(name = "body")
	private String body;
	

	public Integer getId_Template() {
		return id_template;
	}

	public void setId_Template(Integer id_template) {
		this.id_template = id_template;
	}

	public Device getSender() {
		return sender;
	}

	public void setSender(Device sender) {
		this.sender = sender;
	}

	public Device getReceiver() {
		return receiver;
	}

	public void setReceiver(Device receiver) {
		this.receiver = receiver;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
