package com.automation.PushNotif.models;

public class MessageResponse {
	private String messageId;
	private Device sender;
	private Device receiver;
	private String body;
	private Long sentDate;
	private Long receiveDate;

	public MessageResponse(String messageId, Device sender, Device receiver, String body,
                           Long sentDate, Long receiveDate) {
	    this.messageId = messageId;
	    this.sender = sender;
	    this.receiver = receiver;
	    this.body = body;
	    this.sentDate = sentDate;
	    this.receiveDate = receiveDate;
    }

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Device getSender() {
		return sender;
	}

	public void setSender(Device sender) {
		this.sender = sender;
	}

	public Device getReceiver() {
		return receiver;
	}

	public void setReceiver(Device receiver) {
		this.receiver = receiver;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public long getSentDate() {
		return sentDate;
	}

	public void setSentDate(long sentDate) {
		this.sentDate = sentDate;
	}

	public long getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(long receiveDate) {
		this.receiveDate = receiveDate;
	}
}
