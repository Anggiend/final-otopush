package com.automation.PushNotif.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "message")
public class Message {
	@Id
	@Column(name = "id_message")
	private String id_message;

	@Autowired
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_template")
	private Template id_template;
	
	@Column(name = "sent_date")
	private long sentDate;
	
	@Column(name = "receive_date")
	private long receiveDate;


	public String getIdMessage() {
		return id_message;
	}

	public void setIdMessage(String id_message) {
		this.id_message = id_message;
	}

	public Template getIdTemplate() {
		return id_template;
	}

	public void setIdTemplate(Template id_template) {
		this. id_template =  id_template;
	}
	
	public long getSentDate() {
		return sentDate;
	}

	public void setSentDate(long sentDate) {
		this.sentDate = sentDate;
	}

	public long getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(long receiveDate) {
		this.receiveDate = receiveDate;
	}
	
}
