package com.automation.PushNotif.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.automation.PushNotif.models.Message;
import com.automation.PushNotif.models.Template;


public interface MessageRepository extends CrudRepository<Message, Long>{
	
	@Query("Select m From Message m Where m.id_message = ?1")
	Message FindMessageById(String messageId);
	
	@Query("Select m From Message m Where m.id_template = ?1")
	List<Message> FindMessageByIdTemplate(Template templateId);
}
