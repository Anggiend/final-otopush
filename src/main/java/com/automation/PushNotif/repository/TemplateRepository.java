package com.automation.PushNotif.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.automation.PushNotif.models.Device;
import com.automation.PushNotif.models.Message;
import com.automation.PushNotif.models.Template;

public interface TemplateRepository extends CrudRepository<Template, Long>{
	@Query("Select t From Template t Where t.id = ?1")
	Template FindTemplateById(Integer id);
	
	@Query("Select t From Template t Where t.receiver = ?1")
	List<Template> FindTemplateByReceiver(Device device);
}
