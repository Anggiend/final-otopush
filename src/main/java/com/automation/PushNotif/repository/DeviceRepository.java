package com.automation.PushNotif.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.automation.PushNotif.models.Device;

public interface DeviceRepository extends CrudRepository<Device, Long>{
	@Query("Select d From Device d Where d.id = ?1")
	Device FindDeviceById(int id);
	
	@Query("Select d From Device d Where d.token = ?1")
	Device FindDeviceByToken(String token);
}
