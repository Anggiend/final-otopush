package com.automation.PushNotif.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Sort;

import com.automation.PushNotif.models.Message;
import com.automation.PushNotif.models.Template;
import com.automation.PushNotif.repository.MessageRepository;
import com.automation.PushNotif.repository.TemplateRepository;
import com.automation.PushNotif.repository.DeviceRepository;
import com.automation.PushNotif.services.PushNotifService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value="/message")
public class MessageController {
	@Autowired
	TemplateRepository templaterepo;
	@Autowired
	DeviceRepository devicerepo;
	@Autowired
	MessageRepository messagerepo;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/send",method = RequestMethod.POST)
	public boolean SendMessage(@RequestBody String input) {
		ObjectMapper mapper = new ObjectMapper(); // mapper merupakan objek hasil instansiasi dari kelas object mapper 
		JsonNode node = null;
		try{
			node = mapper.readTree(input);
		} catch (IOException e){
			e.printStackTrace();
		}
		
		Integer id = mapper.convertValue(node.get("id"), Integer.class); // ambil parameter input yang dibutuhkan 
		Integer thread = mapper.convertValue(node.get("thread"), Integer.class);
		final Template template = templaterepo.FindTemplateById(id);
		
		if(template == null) {
			return false;
		}
		
		ExecutorService pool = Executors.newFixedThreadPool(thread); // membuat executor untuk mengeksekusi thread sesuai dengan imputan thread
		CountDownLatch latch = new CountDownLatch(thread);// yaitu class yang berfungsi untuk menghitung mundur sesuai dgn jumlah thread
		
		for(int i=0; i < thread ; i++ ){
			Runnable r = new Runnable(){
				public void run(){
					Message message = new Message();
					message.setIdMessage(UUID.randomUUID().toString()); // membuat id random 
					message.setIdTemplate(template);
					message.setSentDate((long)  0);
					message.setReceiveDate((long) 0);
					
					messagerepo.save(message);
					
					PushNotifService pushnotifservice = new PushNotifService();
					pushnotifservice.send(message);
					
					Date now = new Date();
					message.setSentDate(now.getTime());
					messagerepo.save(message);
					
					latch.countDown();//proses mengurangi
				}
			};
			pool.submit(r);
		}
	
		
		try {
			latch.await();
			return true;
		}
		catch (InterruptedException e){
			e.printStackTrace();
			return false;
		}
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/{message_id}/receiveDate",method = RequestMethod.PUT)
	public boolean UpdateReceivedate(@RequestBody String input, @PathVariable String message_id){
		ObjectMapper mapper = new ObjectMapper(); // mapper merupakan objek hasil instansiasi dari kelas object mapper 
		JsonNode node = null;
		
		
		try{
			node = mapper.readTree(input);
		} catch (IOException e){
			e.printStackTrace();
		}

		Long receive_date = mapper.convertValue(node.get("receive_date"), Long.class);
		System.out.println(message_id);
		Message message = messagerepo.FindMessageById(message_id);

		if(message == null){
			return false;
		}


		message.setReceiveDate(receive_date);;
		
		messagerepo.save(message);
		
		return true;
		
	}
}
