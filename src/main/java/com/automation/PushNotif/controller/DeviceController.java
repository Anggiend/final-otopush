package com.automation.PushNotif.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Sort;

import com.automation.PushNotif.models.Message;
import com.automation.PushNotif.models.Template;
import com.automation.PushNotif.models.Device;
import com.automation.PushNotif.models.MessageResponse;
import com.automation.PushNotif.repository.MessageRepository;
import com.automation.PushNotif.repository.TemplateRepository;
import com.automation.PushNotif.repository.DeviceRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value="/device")
public class DeviceController {
	@Autowired
	DeviceRepository deviceRep;
	
	@Autowired
	MessageRepository messageRep;
	
	@Autowired
	TemplateRepository templateRep;
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/",method = RequestMethod.POST) // //new annotation since 4.3
    public Device insertDevice(@RequestBody String input) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		Device device = new Device();
		List<Device> devices = new ArrayList<Device>();
		
		try {
			node = mapper.readTree(input);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String token = mapper.convertValue(node.get("token"), String.class);
		
		devices = (List<Device>) deviceRep.findAll();
		int count = devices.size();
		device.setId(++count);
		device.setToken(token);
		
		deviceRep.save(device);
		return device;
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/{id_device}/receivedMessage",method = RequestMethod.GET) // //new annotation since 4.3
    public List<MessageResponse> getInboxMessage(@PathVariable Integer id_device) {
		Device device = deviceRep.FindDeviceById(id_device);
		
		List<Template> templates = new ArrayList<>();
		templates = templateRep.FindTemplateByReceiver(device);
		
		List<MessageResponse> messageResponses = new ArrayList<>();
		for(Template temp: templates) {
			messageResponses.addAll(converMessageToMessageResponse(messageRep.FindMessageByIdTemplate(temp)));
		}
		
		return messageResponses;
	}

	private List<MessageResponse> converMessageToMessageResponse(List<Message> messages) {
		List<MessageResponse> messageResponseList = new ArrayList<>();
		for(Message message: messages) {
			messageResponseList.add(new MessageResponse(message.getIdMessage(), message.getIdTemplate().getSender(),
					message.getIdTemplate().getReceiver(), message.getIdTemplate().getBody(), message.getSentDate(),
					message.getReceiveDate()));
		}

		return messageResponseList;
	}
	
}
