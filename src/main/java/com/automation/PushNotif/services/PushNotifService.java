package com.automation.PushNotif.services;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import com.automation.PushNotif.models.Message;

public class PushNotifService {
	private static final String FIREBASE_SERVER_KEY = "AAAAbroC_ds:APA91bGsNms8pECjNt0Mo-XeoM8VpymfpgKTsMno3tUcF8y1kg9m5I-7Vgh4MR95ae3wkDZH0TJu_9ru2_kFrgBRYYxBz-MF-b7cRquFmO5qe8OKVDV8hjz9Gfqqr48YbEJsvxFW7tl2";
	private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	
	@Async
	public CompletableFuture<String> send(Message message) {
		JSONObject root = new JSONObject();
        JSONObject notification = new JSONObject();
        JSONObject object = new JSONObject();
        
		object.put("messageId", message.getIdMessage());
        System.out.println(object);
        notification.put("body", message.getIdTemplate().getBody());
        notification.put("click_action","https:localhost:4200");
        notification.put("title", "Notification");

        root.put("data", object);
        root.put("notification", notification);
        root.put("to", message.getIdTemplate().getReceiver().getToken().toString());
        System.out.println(root);
        HttpEntity<String> entity = new HttpEntity<String>(root.toString());
        

		RestTemplate restTemplate = new RestTemplate();
		
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);
 
		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
 
		return CompletableFuture.completedFuture(firebaseResponse);
	}
}
