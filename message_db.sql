-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 18, 2018 at 09:49 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `message_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` varchar(36) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `body` text NOT NULL,
  `sent_date` bigint(20) NOT NULL,
  `receive_date` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `sender`, `receiver`, `body`, `sent_date`, `receive_date`) VALUES
('b370cc59-e6a7-4314-9492-c6cbca6a2435', 1, 3, 'Cek', 1534334682096, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tmp`
--

CREATE TABLE `tmp` (
  `id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `body` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tmp`
--

INSERT INTO `tmp` (`id`, `sender`, `receiver`, `body`) VALUES
(1, 2, 1, 'Hai');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `token` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `token`) VALUES
(1, 'd1zZ7AqTe7M:APA91bFvdE3KT_ZOro3EzBr1VKGCnZf1eEL7iExJFN4gUh4Ol9DtC52quoCP9Sr4ZCqBI4H8SO5XZrmGHvFxlAcv4S7hSvee0898vOagwzRu4L4WAj7v52qgeY1aeFiMsfUOk2DLWrOu'),
(3, 'e5sfw0ju_kM:APA91bG0nltkm9_VI8_ROcywpWDQhSwyzLjx6qFXt5jbc6_0tHtuA3xUwSwfIsneKDVZPt6bBo-8zlSWM0LNalkQ10iftpz6TXKU1jMd6fatOCWRA7-fYrilcKN_6pnQbhvoQ5nuUYAnqKtDv_3krIemtseVRF0ztA'),
(2, 'fWx8XepGrdk:APA91bHZVObyTh0QGQewiuXQ6ycIxtFMIp5rwFKqtuHwAzk8Wr1_x264ar-7w-_QGKKTnBfVgR-h8V1O8Fc0AGFLBopx3XDykHK7AP2FcZfMHe6uCvEKs2hnvHprp6cplekzbahsCJ1BKoATUlovAw4QS20zQwta0w');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `fk_from` (`sender`),
  ADD KEY `fk_to` (`receiver`);

--
-- Indexes for table `tmp`
--
ALTER TABLE `tmp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender` (`sender`),
  ADD KEY `receiver` (`receiver`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ak_user_uk` (`token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tmp`
--
ALTER TABLE `tmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_from` FOREIGN KEY (`sender`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_to` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
